from flask import render_template
from app import app
import random
import signing



@app.route('/main')
def Main():
    dataY = []
    dataX = []
    for iteration in range(90):
        dataY.append(random.randint(1,100))
        dataX.append(iteration)
    user ={'title' : 'AGV', 'nickname' : signing.userName}
    return render_template('main.html', user = user, post = [dataX, dataY])





