import os
from settings import appDB


def ReadCredentials():
	with open(os.path.join(appDB, 'credentials.csv'), 'r') as credentials:
		data = credentials.readlines()
	return {i.split(',')[0]: i.split(',')[1][:-1] for i in data}

def WriteCredentials(userName, passWord):
	with open(os.path.join(appDB, 'credentials.csv'), 'r') as credentials:
		data = credentials.readlines()
		dict = {i.split(',')[0]: i.split(',')[1][:-1] for i in data}
	if userName not in dict:
		with open(os.path.join(appDB, 'credentials.csv'), 'a') as credentials:
			credentials.write(userName + ',' + passWord + '\n')
		return 'account created'
	return 'user name is already existing'
