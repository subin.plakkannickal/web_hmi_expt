import os
# __file__ refers to the file settings.py
appRoot = os.path.dirname(os.path.abspath(__file__))   # refers to application_top
appDB = os.path.join(appRoot, '.temp_db')