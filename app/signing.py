from flask import render_template, redirect, url_for, request
from app import app
import credentials

global userName

@app.route('/', methods=['GET', 'POST'])
@app.route('/sign_in', methods=['GET', 'POST'])
def SignIn():
    global userName
    error = None
    if request.method == 'POST':
        userName = request.form['userName']
        passWord = request.form['passWord']
        credentials_ = credentials.ReadCredentials()
        if userName not in credentials_:
            error = 'Account not found!!!.\tPlease sign up.'
            return render_template('sign_in.html', error=error)
        elif passWord != credentials_[userName]:
            error = 'Invalid Credentials.\tPlease try again.'
            return render_template('sign_in.html', error= error)
        else:
            return redirect(url_for('Main'))
    return render_template('sign_in.html', error=error)


@app.route('/sign_up', methods=['GET', 'POST'])
def SignUp():
    error = None
    if request.method == 'POST':
        name = request.form['name']
        userName = request.form['userName']
        passWordNew = request.form['passWordNew']
        print 'name: ', name, '\n', 'username: ', userName
        rePassWordNew = request.form['rePassWordNew']
        print userName
        if userName == None or userName == '':
            error = 'Invalid user name. Please try again.'
        elif passWordNew != rePassWordNew or passWordNew == '':
            error = 'Passwords are not matching. Please try again.'
        else:
            error = credentials.WriteCredentials(userName, passWordNew)
            if error != 'user name is already existing':
                return redirect(url_for('SignIn'))
    return render_template('sign_up.html', error=error)


@app.route('/recovery')
def ForgotPasswd():
    return render_template('sign_up.html')