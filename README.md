dir map:

	flask
	|- app
	|   |- credentials.py
	|   |- credentials.pyc
	|   |- __init__.py
	|   |- __init__.pyc
	|   |- log_in.py
	|   |- log_in.pyc
	|   |- module.py
	|   |- module.pyc
	|   |- static
	|   |   |- css
	|   |   |   |- index_style.css
	|   |   |   |- login_style.css
	|   |   |- fonts
	|   |   |- js
	|   |- temp_db
	|   |   |- credentials.csv
	|   |- templates
	|       |- index.html
	|       |- log_in.html
	|       |- sign_up.html
	|- README.md
	|- run.py

	

To run:

	python run.py
	
open browser and type

	http://localhost:5000/
	
	