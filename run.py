#!/usr/bin/env python
from app import app

app.run(debug=True, host='localhost')
# app.run(debug=True, host='192.168.1.35')
